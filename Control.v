module ControlUnit(opcode, funct, RegDst, Jump, Branch, BranchNE, slt, MemRead, MemtoReg, ALUOp, MemWrite, ALUSrc, RegWrite);
    input [5:0] opcode, funct;
    output RegDst, Jump, Branch, BranchNE, slt, MemRead, MemtoReg, MemWrite, ALUSrc, RegWrite;
    output [1:0] ALUOp;

    assign RegDst = (opcode == 6'd0);
    assign ALUSrc = (opcode != 6'd0 && opcode != 6'b000100 && opcode != 6'b000101);
    assign MemtoReg = (opcode == 6'b100011);
    assign RegWrite = (opcode != 6'b101011 && opcode[5:1] != 5'b00010 && opcode != 6'b000010);
    assign MemRead = (opcode == 6'b100011);
    assign MemWrite = (opcode == 6'b101011);
    assign Jump = (opcode == 6'b000010);
    assign Branch = (opcode == 6'b000100);
    assign BranchNE = (opcode == 6'b000101);
    assign slt = (opcode == 6'b001010 || (opcode == 6'd0 && funct == 6'b101010));

endmodule;