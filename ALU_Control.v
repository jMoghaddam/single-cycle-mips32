module ALUControl(ALUOp, opcode, funct, aluoperation);
    input [1:0] ALUOp;
    input [5:0] opcode, funct;
    output reg [3:0] aluoperation;

    always @(opcode) begin
        if (opcode == 6'd0) begin
            case(funct)
                6'b100000 : assign aluoperation = 4'b0000;
                6'b100010 : assign aluoperation = 4'b0001;
                6'b100101 : assign aluoperation = 4'b0011;
                6'b100100 : assign aluoperation = 4'b0010;
                6'b101010 : assign aluoperation = 4'b0001;
                default: assign aluoperation = 4'b0000;
            endcase
        end else if(opcode == 6'b001000 || opcode == 6'b100011 || opcode == 6'b101011) begin
            assign aluoperation = 4'b0000;
        end else if(opcode == 6'b001100) begin
            assign aluoperation = 4'b0010;
        end else if(opcode == 6'b001101) begin
            assign aluoperation = 4'b0011;
        end else if(opcode == 6'b001010) begin
            assign aluoperation = 4'b0001;
        end else if(opcode == 6'b000100 || opcode == 6'b000101) begin
            assign aluoperation = 4'b0001;
        end
    end

endmodule;
