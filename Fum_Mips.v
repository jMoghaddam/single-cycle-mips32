module FUM_Mips();

    reg clk;
    reg [31:0] PC;
    wire [31:0] nextPC;
    wire [31:0] PCplus4;
    wire [31:0] PC4plusImm;
    wire [31:0] PCbeq, PCbne, JumpAddress, PCsource1;

    // control unit
    wire RegDst, Jump, Branch, BranchNE, slt, MemRead, MemtoReg, MemWrite, ALUSrc, RegWrite;
    wire [1:0] ALUOp;
    wire [3:0] aluoperation;

    // general
    wire [31:0] ins;
    wire [4:0] wr; 
    wire [31:0] wd;
    wire [31:0] rd1, rd2, finalRd2;
    wire [31:0] aluResult;
    wire zero, lt, gt;
    wire [31:0] rd;
    wire [31:0] imm;

    initial begin
        PC = 32'd0;
    end

    always @(posedge clk) begin
       if (nextPC >= 0) begin
          PC <= nextPC; 
       end
    end

    ControlUnit control(ins[31:26], ins[5:0], RegDst, Jump, Branch, BranchNE, slt, MemRead, MemtoReg, ALUOp, MemWrite, ALUSrc, RegWrite);
    ALUControl aluCtrl(ALUOp, ins[31:26], ins[5:0], aluoperation);
    IMemBank insMem(MemRead, (PC >> 2), ins);
    // MUX2x1 m1(ins[20:16], ins[15:11], RegDst, wr);
    assign wr = RegDst ? ins[15:11] : ins[20:16];

    RegFile registerFile(clk, ins[25:21], ins[20:16], wr, (slt ? {31'd0, lt} : wd), RegWrite, rd1, rd2); 
    // MUX2x1 m2(rd2, imm, ALUSrc, finalRd2);
    assign finalRd2 = ALUSrc ? imm : rd2;

    ALU alu(rd1, finalRd2, aluoperation, aluResult, zero, lt, gt);
    DMemBank dataMem(MemRead, MemWrite, aluResult >> 2, rd2, rd);
    // MUX2x1 m3(aluResult, rd, MemtoReg, wd);
    assign wd = MemtoReg ? rd : aluResult;

    assign imm = {{16{ins[15]}}, ins[15:0]};

    assign PCplus4 = PC + 32'd4;
    assign PC4plusImm = PCplus4 + (imm << 2);
    // MUX2x1 m4(PCplus4, imm << 2, (zero & Branch), PCsource1);
    assign PCbeq = (zero & Branch);
    assign PCbne = (!zero & BranchNE);
    assign PCsource1 = (PCbeq | PCbne) ? PC4plusImm : PCplus4;

    assign JumpAddress = {PCplus4[31:28], {ins[25:0] << 2}};
    // MUX2x1 m4(JumpAddress, PCsource1, Jump, nextPC);
    assign nextPC = Jump ? JumpAddress : PCsource1;

endmodule;
